# FiD project

The goal of this project is to create a simple finger-identification based interaction during the next three hours.
You can augment touch interaction, mouse / trackpad interaction or keyboard interaction, you name it.

## Code
The example code is composed of three parts:
- `sketch-FiD` is a simple Arduino routine that reads a shock sensor (plugged in `GND` and `A0` on an Arduino Uno) value, and sends a simple tag through serial when a shock has been detected.
- `bridge-FiD` is a Python 3 routine that reads the serial output from the Arduino routine, and broadcasts it through websockets.
- `click-demo-FiD.html` is a simple webpage that draws a circle when dragging the mouse, reads a websocket, and display the name of the finger that is perfroming the drag action.

## Useful links
If you're interested in simulating keyboard actions, you can use the `keyboard` Python package (available at [https://github.com/boppreh/keyboard](https://github.com/boppreh/keyboard)).
For instance the following code simulate a key press of the key `a` key.

```
keyboard.press('a')
time.sleep(0.1)
keyboard.release('a')
```

For fancy use of the mouse and trackpad, you can have a look at the `libpointing` open source project (available at [https://github.com/INRIA/libpointing](https://github.com/INRIA/libpointing)).