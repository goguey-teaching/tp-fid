int INDEX_PIEZO_PIN = 0;

int threshold = 1020;
int nb = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  int val_index = analogRead(INDEX_PIEZO_PIN);
  
  if (val_index > threshold) { nb++; Serial.print("INDEX "); Serial.println(nb); }
  
  delay(10);
}
