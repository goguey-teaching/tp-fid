#! /usr/bin/env python3
# # -*- coding: utf-8 -*-

# pip3 install pyserial 
import sys
import getopt
import time, threading
import serial, asyncio, websockets

SERIALPORT = "/dev/cu.usbmodem141101"
SERIALBAUD = 9600

SOCKETHOST = ""
SOCKETPORT = 3000

VERBOSE = 0

#Reading command line
def print_help():
    print('\nUsage:    python bridge.py [[OPTIONS]]')
    print('  -s [arg] or --serial [arg]: path to the glove serial port (default: "{}")'.format(SERIALPORT))
    print('  -b [arg] or --baud [arg]: baudrate used to connect (default: "{}")'.format(SERIALBAUD))
    print('  -p [arg] or --port [arg]: socket server port (default: "{}")'.format(SOCKETPORT))
    print('  -v [arg] or --verbose [arg]: verbose mode (possible values: 0, 1, 2; default: "{}")'.format(SOCKETPORT))
    print('  -h or --help: print help (no argument)')

opts, args = getopt.getopt(
    sys.argv[1:],
    's:b:p:v:h',
    ["serial=", "baud=", "port=", "verbose=", "help"])

for o, v in opts:
    if o in ('-s, --serial'):
        SERIALPORT = v
    if o in ('-b, --baud'):
        try:
            SERIALBAUD = int(v)
        except:
            print('/!\\ Baudrate value should be an integer. /!\\')
            print('   => The default {} value will be used.'.format(SERIALBAUD))
    if o in ('-p, --port'):
        try:
            SOCKETPORT = int(v)
        except:
            print('/!\\ Socket port value should be an integer. /!\\')
            print('   => The default {} value will be used.'.format(SOCKETPORT))
    if o in ('-v, --verbose'):
        try:
            VERBOSE = int(v)
            if VERBOSE < 0: VERBOSE = 0
            if VERBOSE > 2: VERBOSE = 2
        except:
            print('/!\\ Verbose mode value should be 0, 1 or 2. /!\\')
            print('   => The default {} value will be used.'.format(VERBOSE))

    if o in ('-h', '--help'):
        print_help()
        sys.exit()


def connect():
    if VERBOSE > 0:
        print("######## SERIAL INFOS ########")
        print("     PORT:", SERIALPORT)
        print(" BAUDRATE:", SERIALBAUD)
        print("##############################")

    conn = None

    try:
        conn = serial.Serial(
                port     = SERIALPORT,
                baudrate = SERIALBAUD,
                bytesize = 8,
                timeout  = 2,
                stopbits = serial.STOPBITS_ONE
            )
    except Exception as e:
        print("########### ERROR ############")
        print('Error while connecting to the serial port. Please check the settings.')
        print()
        print(e)
        print("##############################")
        conn = None
    
    return conn

class SocketServer:

    async def send(self, ws):
        while len(self.data) > 0:
            print('Sending data...')
            try:
                await ws.send( bytes( self.data[0], 'utf-8'))
                self.data = self.data[1:]
            except:
                print('Sending failed')
                self.data = []

    async def receive(self, websocket, path):
        self.ws = websocket
        while not self.stopped():
            msg = await websocket.recv()
            print('Received from socket client:', msg)

    def pushdata(self, data):
        self.data.append(data)
        asyncio.run_coroutine_threadsafe(self.send(self.ws), self.loop)

    def __init__(self):
        self._thread_running = False
        self._stop = False
        self.ws = None
        self.data = []


    def finished(self):
        return self._thread_running

    def start_server(self):
        if self._thread_running:
            self.stop()
            while self._thread_running:
                time.sleep(0.5)

        self._stop = False
        self._thread_running

        self.loop = asyncio.get_event_loop()
        self.server = websockets.serve(self.receive, SOCKETHOST, SOCKETPORT, max_size=2**32)
        self.loop.run_until_complete(self.server)

        thread = threading.Thread(
            target=self.run_server,
            args=[]
        )
        thread.daemon = True
        thread.start()

    def stop(self):
        self._stop = True

    def stopped(self):
        return self._stop

    def run_server(self):
        if not self.stopped():
            self.loop.run_forever()

        self._thread_running = False

SOCKETSERVER = None
def process_data(data):
    global SOCKETSERVER

    tag = None
    if 'INDEX' in data: tag = 'INDEX'
    if 'MID' in data: tag = 'MID'

    if (tag is not None) and (SOCKETSERVER is not None):
        SOCKETSERVER.pushdata(tag)

def main():
    global SOCKETSERVER

    print("Socket Server initializing...")
    SOCKETSERVER = SocketServer()
    SOCKETSERVER.start_server()

    print("Arduino server initializing...")
    conn = None

    while True:

        print("Arduino server trying to connect...")
        conn = connect()
        if conn == None:
            print(" => Connection Failed. Please check wire and serial options.")
            time.sleep(1)
            continue

        print("Arduino server running")
        while conn.is_open:
            try:
                if(conn.in_waiting > 0):
                    data = None
                    try:
                        data = conn.readline().decode('Ascii')
                    except:
                        break

                    if VERBOSE > 0:
                        print("Data received:")
                        print(">", data)
                    process_data(data)

            except:
                time.sleep(1)
                break

        conn = None
        print("Arduino server closed.")

    sclient.stop()
    sclient.join()


if __name__ == '__main__':
    main()
